<html>
    <head>
        <title>Mentions légales</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Mentionslegales.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
    </head>
    <body>
        <?php
        ?>
        <nav>
            <ul class="menu">
                <li>
                    <a href="../vue/Flexbox.html"target="_blank"><span>ACCUEIL<!-- Mettre lien accueil --></span></a>
                </li>
                <li>
                    <a href="Mentionslegales.php"target="_blank"><span>NOTRE HISTOIRE<!--Mettre lien vers paragraphe histoire--></span></a>
                </li>
                <li>
                    <a href="Mentionslegales.php"target="_blank"><span>NOTRE SAVOIR-FAIRE<!--mettre lien notre savoir faire--></span></a>
                </li>
                <li>
                    <a href="Mentionslegales.php"target="_blank"><span>NOS PRODUITS<!--mettre lien vers nos produits--></span></a>
                </li>
                <li>
                    <a href="Mentionslegales.php"target="_blank"><span>CONTACT<!--mettre lien contact--></span></a>
                </li>
            </ul>


        </nav>
        <header>
            <h1>MENTIONS LEGALES</h1>
        </header>
        <main>
            <h5>LOREM</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut diam pulvinar, ultricies turpis quis, congue eros. Quisque augue turpis, varius ac tincidunt id, fringilla non dolor. Sed sed turpis diam. Phasellus scelerisque mauris vitae magna tempor, nec luctus est accumsan. Fusce pellentesque libero a massa varius, nec lacinia tortor placerat. Phasellus molestie magna sed congue dignissim. Pellentesque id convallis lacus. Phasellus viverra velit pretium ante finibus vulputate. Curabitur feugiat, erat id ultrices malesuada, eros augue bibendum lorem, sed tempor elit ex sit amet sapien. Nam faucibus mi et ipsum hendrerit cursus. Nunc erat erat, finibus mattis est in, viverra sodales orci.</p>
            <h5>LOREM</h5>
            <p>Proin quis blandit erat. Curabitur euismod efficitur felis vitae vestibulum. Nullam in massa id sapien luctus commodo. Praesent eget hendrerit lectus. Mauris vehicula blandit cursus. Donec eu luctus nulla. Proin neque erat, ultricies ut est in, posuere pretium tortor. Suspendisse faucibus id enim sit amet dignissim.</p>
            <h5>LOREM</h5>
            <p>Aliquam rutrum mauris leo. Sed quis rutrum neque. Aliquam erat volutpat. Sed gravida in nunc a maximus. Aenean et venenatis ligula. Cras sollicitudin sollicitudin enim, vitae tristique diam convallis bibendum. Duis eros orci, fermentum id malesuada nec, facilisis venenatis odio.</p>
            <h5>LOREM</h5>
            <p>Mauris id venenatis orci. Etiam id nisl ac nulla laoreet sagittis. Donec bibendum faucibus metus id malesuada. Mauris nisi nisl, sagittis sed eleifend sed, blandit quis felis. Quisque sollicitudin ex nec facilisis auctor. Suspendisse ut bibendum tortor. Cras ligula lectus, facilisis sit amet nibh vitae, tempor suscipit dolor</p>
            <h5>LOREM</h5>
            <p>Donec posuere tortor libero, vitae vehicula neque tincidunt nec. Aenean sollicitudin ut orci nec mollis. Nam non leo tristique, convallis tellus sit amet, gravida velit. Duis sem nulla, eleifend id quam sit amet, lacinia gravida lorem. Mauris vehicula libero eget massa blandit pretium. Aliquam vitae est commodo eros volutpat hendrerit. Integer vitae turpis non nulla malesuada accumsan a interdum nisl. Aliquam at tincidunt odio, eget aliquet tortor.</p>
            <img src="../images/Apu-600x450.jpg">

        </main>

        <footer>
            <ul id="footer1">

                LOREM IPSUM


                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
            </ul>

            <ul id="footer2">

                LOREM IPSUM


                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
            </ul>

            <ul id="footer3">

                LOREM IPSUM


                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
                <li>
                    <a href="Mentionslegales.php">lorem</a>
                </li>
            </ul>
            <ul id="footer4">SERVICE CLIENT
                <li id="tel">DU LUNDI AU JEUDI DE 8H À 18H ET LE VENDREDI DE 8H À 17H</li>

                <li>
                    <a id="btn_message" href="Mentionslegales.php"target="_blank">Nous contacter</a>
                </li>
            </ul>

        </footer>
    </body>
