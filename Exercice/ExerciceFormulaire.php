<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        ?>
        <strong>Création de votre compte utilisateur</strong><br>
        <table border="1px">
            <form action="ExerciceFormulaire.php"method="POST">
                <tr>
                    <td><label>Nom d'utilisateur<sup>(*)</sup></label>:
                    </td>
                    <td>
                        <input id="pseudo"type="text"name="id"placeholder="Saisir un nom d'utilisateur"required pattern="[a-zA-Z0-9]{6,}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Mot de passe <sup>(*)</sup></label>:
                    </td>
                    <td>
                        <input id="motdepasse"type="password"name="motdepasse"placeholder="De 6 à 12 caractères"pattern="[a-zA-Z0-9]{6,12}"required autocomplete="off">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Adresse email <sup>(*)</sup></label>:
                    </td>
                    <td>
                        <input  type="email"id="email"required autocomplete="off"value="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Confirmez votre email <sup>(*)</sup></label>:
                    </td>
                    <td>
                        <input type="email"id="email2"autocomplete="off"required oninput="verifEmail()">
                    </td>
                </tr>
        </table>
        <details>
            <summary>Informations complémentaires :</summary>
            <table border="1px">
                <tr>
                    <td>
                        <label>Nom :</label>
                    </td>
                    <td>
                        <input id="nom"type="text"name="nom"required pattern="[a-zA-Z'-]{1,}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Prénom :</label>
                    </td>
                    <td>
                        <input id="prénom"type="text"name="prénom"pattern="[a-zA-Zâ€™-]{0,}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Date de naissance :</label>
                    </td>
                    <td>
                        <input type="date"name="birthday"id="birthday"min="1920-01-01"max="">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Téléphone :</label>
                    </td>
                    <td>
                        <input id="telephone"type="tel"name="telephone"pattern="[0-9]{2}.[0-9]{2}.[0-9]{2}.[0-9]{2}.[0-9]{2}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Loisir préféré :</label>
                    </td>
                    <td>
                        <datalist id="loisirs">
                            <option value="Musique">
                            <option value="Cinéma">
                            <option value="Théatre">
                            <option value="Sport">
                            <option value="Restaurant">
                        </datalist>
                        <input id="loisirs"type="text"name="f_loisirs"list="loisirs"spellcheck="true">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Couleur :</label>
                    </td>
                    <td>
                        <input id="couleur"type="color"name="couleur"value="#ff0000">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Donne une note au site :</label>
                    </td>
                    <td>
                        <input id="note"type="range"name="note"min="0"max="10"value="0" oninput="document.getElementById('resultat').innerHTML=document.getElementById('note').value" ><strong><span id="resultat">0</span> / 10</strong>
                    </td>
                </tr>
            </table>
            <tr>
                <td>
                    <textarea id="commentaires"name="commentaires"placeholder="Place aux commentaires..."rows="10"cols="100"></textarea>
                </td>
            </tr>

        </details>
        <input type="submit"name="valider"formnovalidate="formnovalidate"value="Enregistrer et valider plus tard">
        <input type="submit"name="valider"value="Valider">
    </form>
    <hr>
    <script>
        function verifEmail() {

            var email2 = document.getElementById('email2');

            if (document.getElementById('email').value != email2.value) {

                return email2.setCustomValidity('Les deux adresses e-mail doivent être identiques');
            }

            email2.setCustomValidity('');
        }
    </script>
</body>

